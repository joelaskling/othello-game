﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using Common;

namespace OthelloServer
{
    /// <summary>
    /// This class implements the OthelloServer.
    /// </summary>
    public class Server
    {
        IPAddress address = Dns.GetHostAddresses(Dns.GetHostName())[1];
        int port = 8000;
        TcpListener server;

        public Server()
        {
            server = new TcpListener(address, port);
            server.Start();
            DatabaseManager db = new DatabaseManager();
            db.SetConnectionInfo(address.ToString(), port);
        }
        /// <summary>
        /// This method accepts and handels a client. It receives the number of available moves from the client,
        /// chooses one at random and writes it back to the client.
        /// </summary>
        public void Run()
        {
            while (true)
            {
                TcpClient client = server.AcceptTcpClient();
                Thread clientThread = new Thread(() =>
                {
                    try
                    {
                        BinaryReader reader = new BinaryReader(client.GetStream());
                        BinaryWriter writer = new BinaryWriter(client.GetStream());
                        while (true)
                        {
                            Random random = new Random();
                            int numberOfmoves = reader.Read();
                            int move = random.Next(0, numberOfmoves);
                            writer.Write(move);
                        }
                    }
                    catch (IOException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                });
                clientThread.Start();
            }
        }
    }
}
