﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Common
{
    /// <summary>
    /// Connects to a Database to retrive/Set connection information to the server.
    /// </summary>
    public class DatabaseManager
    {
        public string IPAddress { get; set; }
        public int portNr { get; set; }
        private SqlConnection connection;
        private string connectionString = @"Server=(localdb)\mssqllocaldb;Database=ArenaDb;Trusted_Connection=True;MultipleActiveResultSets=true";
        /// <summary>
        /// Used by Client to get the servers connection information from the Database.
        /// </summary>
        public void GetConnectionInfo()
        {
            using (connection = new SqlConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();
                string procedure = "EXEC GetConnectionInfo @GroupId = 4;";
                SqlCommand myCommand = new SqlCommand(procedure, connection);

                using (SqlDataReader dataReader = myCommand.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        IPAddress = dataReader["IPAddress"].ToString();
                        portNr = (int)dataReader["PortNr"];
                    }
                }
            }
        }
        /// <summary>
        /// Used by the server to set the connection information to the Database.
        /// </summary>
        /// <param name="IPAddress"></param>
        /// <param name="PortNr"></param>
        public void SetConnectionInfo(string IPAddress, int PortNr)
        {
            using (connection = new SqlConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();
                string command = $"UPDATE OthelloServer SET IPAddress = '{IPAddress}', PortNr = {PortNr} WHERE GroupId = 4;";
                Console.WriteLine(command);
                SqlCommand myCommand = new SqlCommand(command, connection);
                myCommand.ExecuteNonQuery();
            }
        }
    }
}
