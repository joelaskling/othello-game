﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OthelloGame.Model
{
    /// <summary>
    /// This is an abstract class that is inherited by each playertype
    /// </summary>
    public abstract class Player
    {
        public string name { get; set; }
        public int[] move { get; set; }
        public MarkerID markerId { get; set; }

        public abstract int[] Move(List<List<int[]>> AllowedMoves, ManualResetEvent mre);
    }
}
