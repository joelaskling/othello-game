﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Common;

namespace OthelloGame.Model
{
    /// <summary>
    /// This class is used by the RemoteComputerPlayer to connect to the OthelloServer.
    /// </summary>
    public class Client
    {
        string host;
        int port;
        BinaryReader reader;
        BinaryWriter writer;
        TcpClient client;
        DatabaseManager db;

        public Client()
        {
            db = new DatabaseManager();
            db.GetConnectionInfo();
            host = db.IPAddress;
            port = db.portNr;
            client = new TcpClient(host, port);
            reader = new BinaryReader(client.GetStream());
            writer = new BinaryWriter(client.GetStream());
        }
        public int GetMoveFromServer(int NumberOfAllowedMoves)
        {
            writer.Write(NumberOfAllowedMoves);
            writer.Flush();
            int moveFromServer = reader.Read();
            return moveFromServer;
        }
    }
}
