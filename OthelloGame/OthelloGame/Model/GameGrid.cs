﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using OthelloGame.Control;

namespace OthelloGame.Model
{
    /// <summary>
    /// This Class contains a matrix reprecenting the logical gamegrid, it provides all the logic behind available moves, current boardstate
    /// and updating to a new state. the class also inherits from the subjekt class to notify GameBoard of changes.
    /// </summary>
    public class GameGrid : Subject
    {
        /// <summary>This is a matrix reprecenting the board</summary>
        public MarkerID[,] board { get; set; }
        /// <summary>This is the white marks in the board</summary>
        public List<int[]> WhiteMarks { get; set; }
        /// <summary>This is the black marks on the board</summary>
        public List<int[]> BlackMarks { get; set; }


        public GameGrid()
        {
            board = new MarkerID[10, 10];
            WhiteMarks = new List<int[]>();
            BlackMarks = new List<int[]>();

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    board[i, j] = MarkerID.unsigned;
                }
            }
        }
        /// <summary>
        /// This method updates the gameGrid and notifies any observers of the changes.
        /// </summary>
        /// <param name="coords">coordinates of the mark to change</param>
        /// <param name="player">The marker color of the current player</param>
        public void UpdateBoard(int[] coords, MarkerID player)
        {
            if (player == MarkerID.white)
            {
                if (!IfInMarkList(coords, WhiteMarks))
                    WhiteMarks.Add(coords);

                if (board[coords[0], coords[1]] == MarkerID.black)
                {
                    RemoveMarkFromList(coords, BlackMarks);
                }
            }

            else
            {
                if (!IfInMarkList(coords, BlackMarks))
                    BlackMarks.Add(coords);

                if (board[coords[0], coords[1]] == MarkerID.white)
                {
                    RemoveMarkFromList(coords, WhiteMarks);
                }
            }

            board[coords[0], coords[1]] = player;
            int[] tmp = { coords[0] - 1, coords[1] - 1 };
            Notify(tmp, player);
        }
        /// <summary>
        /// This method is used to check if a mark is on the board.
        /// </summary>
        /// <param name="coords">Coords of the mark</param>
        /// <param name="markList">List of marks, either white or black</param>
        /// <returns></returns>
        private bool IfInMarkList(int[] coords, List<int[]> markList)
        {
            foreach (var mark in markList)
            {
                if (Enumerable.SequenceEqual(mark, coords))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// This method removes a mark from the board.
        /// </summary>
        /// <param name="coords">Coords of the mark</param>
        /// <param name="markList">List of marks, either white or black</param>
        private void RemoveMarkFromList(int[] coords, List<int[]> markList)
        {
            foreach (var mark in markList)
            {
                if (Enumerable.SequenceEqual(mark, coords))
                {
                    markList.Remove(mark);
                    break;
                }
            }
        }
        /// <summary>
        /// This method is used to find all available moves.
        /// </summary>
        /// <param name="player">The current player</param>
        /// <param name="opponent">The current opponent</param>
        /// <returns>A list of all available moves, the moves are represented by lists of marks that would be affected
        /// by that move and every mark in that list is represented by coordinates.</returns>
        public List<List<int[]>> AvailableMoves(Player player, Player opponent)
        {
            List<int[]> playerMarks = new List<int[]>();
            List<List<int[]>> AllowedMoves = new List<List<int[]>>();

            if (player.markerId == MarkerID.white)
                playerMarks = WhiteMarks;
            else
                playerMarks = BlackMarks;

            foreach (var mark in playerMarks)
            {
                List<int[]> potentialMoves = new List<int[]>();

                for (int i = mark[0] - 1; i <= mark[0] + 1; i++)
                {
                    for (int j = mark[1] - 1; j <= mark[1] + 1; j++)
                    {
                        if (board[i, j] == opponent.markerId)
                        {
                            int[] temp = { i, j };
                            potentialMoves.Add(temp);
                        }
                    }
                }

                if (potentialMoves.Count != 0)
                {
                    foreach (var move in potentialMoves)
                    {
                        int iTo, iFrom = move[0], jTo, jFrom = move[1];
                        bool allowedMove = false;
                        bool allowed = true;

                        List<int[]> AllowedMove = new List<int[]>();
                        AllowedMove.Add(mark);

                        while (true)
                        {
                            int[] temp = { iFrom, jFrom };
                            AllowedMove.Add(temp);

                            if (allowedMove)
                                break;

                            if (mark[0] > move[0] && mark[1] == move[1])
                            {
                                iTo = 1; jTo = jFrom; iFrom--;
                            }
                            else if (mark[0] < move[0] && mark[1] == move[1])
                            {
                                iTo = 9; jTo = jFrom; iFrom++;
                            }
                            else if (mark[0] == move[0] && mark[1] > move[1])
                            {
                                jTo = 1; iTo = iFrom; jFrom--;
                            }
                            else if (mark[0] == move[0] && mark[1] < move[1])
                            {
                                jTo = 9; iTo = iFrom; jFrom++;
                            }
                            else if (mark[0] < move[0] && mark[1] < move[1])
                            {
                                jTo = 9; iTo = 9; jFrom++; iFrom++;
                            }
                            else if (mark[0] > move[0] && mark[1] > move[1])
                            {
                                jTo = 1; iTo = 1; jFrom--; iFrom--;
                            }
                            else if (mark[0] > move[0] && mark[1] < move[1])
                            {
                                jTo = 9; iTo = 1; jFrom++; iFrom--;
                            }
                            else if (mark[0] < move[0] && mark[1] > move[1])
                            {
                                jTo = 1; iTo = 9; jFrom--; iFrom++;
                            }

                            if (board[iFrom, jFrom] == MarkerID.unsigned)
                                allowedMove = true;

                            if (board[iFrom, jFrom] == player.markerId)
                                allowed = false;
                        }
                        if (allowed)
                        {
                            if (iFrom != 0 && jFrom != 0 && iFrom != 9 && jFrom != 9)
                                AllowedMoves.Add(AllowedMove);
                        }
                    }
                }
            }
            return AllowedMoves;
        }
    }
}
