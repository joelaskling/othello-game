﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;

namespace OthelloGame.Model
{
    /// <summary>
    /// This class implements a remote Computer player and it inherits from Player
    /// </summary>
    public class RemoteComputerPlayer : Player 
    {
        Client client;
        /// <summary>
        /// RemoteComputerPlayer sets up a connection to the OthelloServer by creating a new Client
        /// </summary>
        /// <param name="name">Player name</param>
        public RemoteComputerPlayer(string name)
        {
            this.name = name;
            client = new Client();
        }
        /// <summary>
        /// This method overrides Player's method Move() and is used by GameManager to prompt the player to make a move.
        /// It requests a move from the server and returns it.
        /// </summary>
        /// <param name="AllowedMoves">All allowed moves</param>
        /// <param name="mre">ManualResetEvent set by GameBoard (not used by remote computer)</param>
        /// <returns></returns>
        public override int[] Move(List<List<int[]>> AllowedMoves, ManualResetEvent mre)
        {
            int numberOfAllowedMoves = AllowedMoves.Count - 1;
            int moveFromServer = client.GetMoveFromServer(numberOfAllowedMoves);
            return AllowedMoves[moveFromServer][AllowedMoves[moveFromServer].Count - 1];
        }
    }
}
