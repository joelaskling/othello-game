﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OthelloGame.Model
{
    /// <summary>
    /// This class implements a Local Computer player and it inherits from Player
    /// </summary>
    public class LocalComputerPlayer : Player
    {
        public LocalComputerPlayer(string name)
        {
            this.name = name;
        }
        /// <summary>
        /// This method overrides Player's method Move() and is used by GameManager to prompt the player to move.
        /// Local Computer Chooses the move where most markers are turned and returns it to GameManager.
        /// </summary>
        /// <param name="AllowedMoves">All allowed moves</param>
        /// <param name="mre">ManualResetEvent set by GameBoard (not used by local computer)</param>
        /// <returns>The choosen move</returns>
        public override int[] Move(List<List<int[]>> AllowedMoves, ManualResetEvent mre)
        {
            Thread.Sleep(50);
            int count = 0;
            List<int[]> bestMove = new List<int[]>();
            foreach (var move in AllowedMoves)
            {
                if (count < move.Count)
                {
                    count = move.Count;
                    bestMove = move;
                }
            }
            return bestMove[count - 1];
        }
    }
}
