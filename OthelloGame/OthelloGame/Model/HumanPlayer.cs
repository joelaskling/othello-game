﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OthelloGame.Model
{
    /// <summary>
    /// This class implements a Human player and it inherits from Player
    /// </summary>
    public class HumanPlayer : Player
    {
        public HumanPlayer(string name)
        {
            this.name = name;
        }
        /// <summary>
        /// This method overrides Player's method Move() and is used by GameManager to promt the players move.
        /// A ManualResetEvent set by GameBoard signals when the player has made a move and the method should return.
        /// </summary>
        /// <param name="AllowedMoves">All allowed moves (not used by human player)</param>
        /// <param name="mre">ManualResetEvent set by GameBoard</param>
        /// <returns></returns>
        public override int[] Move(List<List<int[]>> AllowedMoves, ManualResetEvent mre)
        {
            mre.WaitOne();
            mre.Reset();
            return null;
        } 
    }
}
