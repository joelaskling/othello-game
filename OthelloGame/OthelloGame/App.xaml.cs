﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using OthelloGame.Control;
using OthelloGame.View;
using System.Threading;

namespace OthelloGame
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// This Class contains the main Method
    /// </summary>
    public partial class App : Application
    {
        App()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The Main method creates and shows the MainWindow and creates a new thread where the 
        /// GameManager is created and set up and manages the game.
        /// </summary>
        [STAThread()]
        static void Main()
        {
            App app = new App();
            MainWindow View = new MainWindow();
            View.Show();
            
            // Eftersom Gameframe (vårt Mainwondow) skall vara uppe under hela programmets körning och
            // inte fungerar särskilt bra med .ShowDialog() har vi Nedan skapat en ny tråd med en loop 
            // där programmet väntar på att användaren antingen skall Skapa ett nytt parti eller avsluta.
            new Thread(() =>
            {
                while (true)
                { 
                    View.mre.WaitOne(); 
                    View.mre.Reset();
                    GameManager gameManager = new GameManager(View.gameBoard, View.setUp);
                    
                }
            }).Start();
               
            app.Run();

        }
    }
}
