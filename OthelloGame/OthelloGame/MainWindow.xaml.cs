﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OthelloGame.View;

namespace OthelloGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>Attributes of SetupGameDialog is used by GameManager to set up game/summary>
        public SetupGameDialog setUp;
        /// <summary>Gameboard is used by GameManager to show an instance of the GameBoard/summary>
        public GameBoard gameBoard;
        /// <summary>ManualResetEvent is used to signal the Main method When NewGameBtn has been clicked/summary>
        public ManualResetEvent mre = new ManualResetEvent(false);
        public MainWindow()
        {
            InitializeComponent();
        }
        private void NewGameBtn_Click(object sender, RoutedEventArgs e)
        {
            setUp = new SetupGameDialog(mre);
            gameBoard = new GameBoard();
            setUp.ShowDialog();
        }

        private void QuitBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Environment.Exit(1);
        }

        private void OnExit(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Environment.Exit(1);
        }
    }
}
