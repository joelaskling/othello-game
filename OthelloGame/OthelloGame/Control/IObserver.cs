﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;


namespace OthelloGame.Control
{
    /// <summary>
    /// This is an interface following the Observer design strategy.
    /// </summary>
    public interface IObserver
    {
        public abstract void Update(int[] coords, MarkerID player);
    }
}
