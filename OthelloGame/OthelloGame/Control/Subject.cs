﻿using OthelloGame.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace OthelloGame.Control
{
    /// <summary>
    /// This is an abstract class containing methods to attach, detach and notify observers
    /// using the IObserver interface.
    /// </summary>
    public abstract class Subject
    {
        private List<IObserver> observers = new List<IObserver>();
        /// <summary>
        /// This method is used to attach an observer to the list of observers
        /// </summary>
        /// <param name="observer"></param>
        public void Attach(IObserver observer)
        {
            observers.Add(observer);
        }
        /// <summary>
        /// This method is used to Detach an observer from the list of observers
        /// </summary>
        /// <param name="observer"></param>
        public void Detach(IObserver observer)
        {
            observers.Remove(observer);
        }
        /// <summary>
        /// This method is used to notify the observers in the list of changes in the subjekt. 
        /// </summary>
        /// <param name="coords"></param>
        /// <param name="player"></param>
        public void Notify(int[] coords, MarkerID player)
        {
            foreach (IObserver observer in observers)
            {
                observer.Update(coords, player);
            }
        }
    }
}
