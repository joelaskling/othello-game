﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Windows;
using OthelloGame.View;
using OthelloGame.Model;
using System.Net.Sockets;
using Common;

namespace OthelloGame.Control
{
    /// <summary>
    /// This class creates and controls the game.
    /// </summary>
    public class GameManager
    {
        Player playerWhite;
        Player playerBlack;
        Player currentPlayer;
        GameGrid gameGrid;
        GameBoard gameBoard;
        EndGameDialog endGame;
        string endGameContent = " ";
        int[] startCord1 = { 4, 4 };
        int[] startCord2 = { 5, 5 };
        int[] startCord3 = { 4, 5 };
        int[] startCord4 = { 5, 4 };

        /// <summary>
        /// GameManager creates a gamegrid and attaches gameBoard as an observer via the abstract class
        /// Subjekt. gameGrid is then updated with the position of the starting markers and the propertis 
        /// of setUp is used to set up the game. If no errors occur during setup GameManager manages the game 
        /// with the method ManageGame.
        /// </summary>
        /// <param name="gameBoard">The visual board</param>
        /// <param name="setUp">The information about each player</param>
        public GameManager(GameBoard gameBoard, SetupGameDialog setUp)
        {
            this.gameBoard = gameBoard;
            gameGrid = new GameGrid();
            gameGrid.Attach(gameBoard);
            gameGrid.UpdateBoard(startCord1, MarkerID.white);
            gameGrid.UpdateBoard(startCord2, MarkerID.white);
            gameGrid.UpdateBoard(startCord3, MarkerID.black);
            gameGrid.UpdateBoard(startCord4, MarkerID.black);
            SetupGame(setUp);

        }
        /// <summary>
        /// This Method Manages the game by prompting either player1 or player2 to move using the Move() method
        /// of each player. If no player can make a move The EndGameDialog is created and shown presenting the results.
        /// </summary>
        private void ManageGame()
        {
            gameBoard.Dispatcher.Invoke(() => gameBoard.Show());
            int countAvailableMoves = 0;
            while (true)
            {
                List<List<int[]>> AllowedMoves;
                if (countAvailableMoves == 2)
                    break;

                if (currentPlayer.markerId == MarkerID.white)
                {
                    AllowedMoves = gameGrid.AvailableMoves(playerWhite, playerBlack);
                    if (AllowedMoves.Count != 0)
                    {
                        // Här hämtas koordinaterna från respektive spelartyp, men eftersom mänsklig spelare inte har tillgång
                        // till GameBoard returnerar mänsklig spelare null och koordinatrna hämtas av Gamemanager direkt från GameBoard.
                        int[] coords = playerWhite.Move(AllowedMoves, gameBoard.mre);
                        if (coords == null)
                            coords = gameBoard.coords;
                        HandlePlayer(playerWhite, playerBlack, coords, AllowedMoves);
                        countAvailableMoves = 0;
                    }
                    else
                    {
                        currentPlayer = playerBlack;
                        countAvailableMoves++;
                    }
                }
                else
                {
                    AllowedMoves = gameGrid.AvailableMoves(playerBlack, playerWhite);
                    if (AllowedMoves.Count != 0)
                    {
                        int[] coords = playerBlack.Move(AllowedMoves, gameBoard.mre);
                        if (coords == null)
                            coords = gameBoard.coords;
                        HandlePlayer(playerBlack, playerWhite, coords, AllowedMoves);
                        countAvailableMoves = 0;
                    }
                    else
                    {
                        currentPlayer = playerWhite;
                        countAvailableMoves++;
                    }
                }  
            }
            gameBoard.Dispatcher.Invoke(() =>
            {
                if (gameGrid.BlackMarks.Count > gameGrid.WhiteMarks.Count)
                    endGameContent = $"{playerBlack.name} won with {gameGrid.BlackMarks.Count} markers against {playerWhite.name} with {gameGrid.WhiteMarks.Count} markers";
                else if (gameGrid.BlackMarks.Count < gameGrid.WhiteMarks.Count)
                    endGameContent = $"{playerWhite.name} won with {gameGrid.WhiteMarks.Count} markers against {playerBlack.name} with {gameGrid.BlackMarks.Count} markers";
                else
                    endGameContent = "Draw";
                endGame = new EndGameDialog(endGameContent);
                endGame.ShowDialog();
                gameBoard.Close();
            });
        }
        /// <summary>
        /// This method checks if the player move is allowed, updates the gameGrid and switches currentPlayer. 
        /// If the move is not allowed the player is notified.
        /// </summary>
        /// <param name="player">The current player</param>
        /// <param name="opponent">The current opponent</param>
        /// <param name="coords">The move Choosen by the player</param>
        /// <param name="AllowedMoves">All allowed moves</param>
        private void HandlePlayer(Player player, Player opponent, int[] coords, List<List<int[]>> AllowedMoves)
        {
            bool moveAllowed = false;
            foreach (var move in AllowedMoves)
            {
                if (Enumerable.SequenceEqual(move[move.Count - 1], coords))
                {
                    foreach (var mark in move)
                    {
                        gameGrid.UpdateBoard(mark, player.markerId);
                    }
                    moveAllowed = true;
                }
            }
            currentPlayer = player;

            if (!moveAllowed)
            {
                gameBoard.Dispatcher.Invoke(() => gameBoard.statBarText.Text = $"Player {player.markerId}, Your move is not allowed");
            }
            else
            {
                gameBoard.Dispatcher.Invoke(() => gameBoard.statBarText.Text = $"Player {opponent.markerId}");
                currentPlayer = opponent;
            }
        }
        /// <summary>
        /// This method uses the information from SetupGameDialog to create players.
        /// </summary>
        /// <param name="setUp">Class instance that contains the player information as properties</param>
        private void SetupGame(SetupGameDialog setUp)
        {
            try
            {
                if (setUp.PlayerOneWhite)
                {
                    playerWhite = InitializePlayers(setUp.player1, setUp.PlayerOneName);
                    playerBlack = InitializePlayers(setUp.player2, setUp.PlayerTwoName);
                }
                else
                {
                    playerWhite = InitializePlayers(setUp.player2, setUp.PlayerTwoName);
                    playerBlack = InitializePlayers(setUp.player1, setUp.PlayerOneName);
                }
                playerWhite.markerId = MarkerID.white;
                playerBlack.markerId = MarkerID.black;
                currentPlayer = playerWhite;
                ManageGame();
            }
            catch (SocketException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Helper method to SetupGame to reduce code duplication.
        /// </summary>
        /// <param name="playerType"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private Player InitializePlayers(string playerType, string name)
        {
            Player player = null;
            if (playerType == "Human")
            {
                player = new HumanPlayer(name);
            }
            else if (playerType == "Computer")
            {
                player = new LocalComputerPlayer(name);
            }
            else
            {
                player = new RemoteComputerPlayer(name);
            }
            return player;
        }
    }
}
