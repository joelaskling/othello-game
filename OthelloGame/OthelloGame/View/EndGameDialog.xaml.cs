﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OthelloGame.View
{
    /// <summary>
    /// Interaction logic for EndGameDialog.xaml
    /// EndGameDialog shows the result of the game to the user.
    /// </summary>
    public partial class EndGameDialog : Window
    {
        public EndGameDialog(string labelcontent)
        {
            InitializeComponent();
            LabelContent.Content = labelcontent;
        }
        private void OkBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
