﻿using OthelloGame.Control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using Common;

namespace OthelloGame.View
{
    /// <summary>
    /// Interaction logic for GameBoard.xaml
    /// GameBoard creates a visual representation of the game to a human user. It also handles the
    /// human interaktion with the game by either mouse-clicks or Enter-key. GameBoard uses the interface
    /// IObserver and implement its method "Update" to update the colors of the board from the current GameGrid
    /// without actually "knowing" the class GameGrid.
    /// </summary>
    public partial class GameBoard : Window, IObserver
    {
        /// <summary>ManualResetEvent is used to signal GameManager that a human player has made a move</summary>
        public ManualResetEvent mre { get; set; }
        /// <summary>Used by GameManager to send the cords of a human player move to GameGrid</summary>
        public int[] coords { get; set; }
        public GameBoard()
        {
            mre = new ManualResetEvent(false);
            InitializeComponent();
        }

        public void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            int row = (int)btn.GetValue(Grid.RowProperty);
            int column = (int)btn.GetValue(Grid.ColumnProperty);
            int[] tmp = { column + 1, row + 1 };
            coords = tmp;
            mre.Set();
        }

        public void Update(int[] coords, MarkerID player)
        {
            if (player == MarkerID.black)
            {
                ChangeColor(coords[0], coords[1], MarkerID.black);
            }
            else if (player == MarkerID.white)
            {
                ChangeColor(coords[0], coords[1], MarkerID.white);
            }
        }

        public void ChangeColor(int x, int y, MarkerID markerID)
        {
            this.Dispatcher.Invoke(() =>
            {
                Button btn = (Button)VisualBoard.Children.Cast<UIElement>().FirstOrDefault(a => Grid.GetColumn(a) == x && Grid.GetRow(a) == y);
                if (markerID == MarkerID.white)
                    btn.Background = Brushes.White;
                else
                    btn.Background = Brushes.Black;
            });
        }

        private void Button_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Button btn = sender as Button;

                int row = (int)btn.GetValue(Grid.RowProperty);
                int column = (int)btn.GetValue(Grid.ColumnProperty);
                int[] tmp = { column + 1, row + 1 };
                coords = tmp;
                mre.Set();
            }
        }
    }
}
