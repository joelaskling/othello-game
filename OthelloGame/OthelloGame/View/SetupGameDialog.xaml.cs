﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OthelloGame.Control;

namespace OthelloGame.View
{
    /// <summary>
    /// Interaction logic for SetupGameDialog.xaml
    /// SetupGameDialog retrives, verifies and saves the player information as properties for GameManager to use
    /// When creating a game.
    /// </summary>
    public partial class SetupGameDialog : Window
    {
        bool colorChecked = false;
        public string player1 { get; set; }
        public string player2 { get; set; }
        public bool PlayerOneWhite = false;
        public string PlayerOneName { get; set; }
        public string PlayerTwoName { get; set; }
        public ManualResetEvent mre;
        /// <summary>
        /// The constructor takes a ManualResetEvent from Mainwindow to signal The Main method when the
        /// Player information is set so the GameManager can be created.
        /// </summary>
        /// <param name="mre"></param>
        public SetupGameDialog(ManualResetEvent mre)
        {
            this.mre = mre;
            InitializeComponent();
        }

        private void PlayerOneWhiteRadioBtn_Click(object sender, RoutedEventArgs e)
        {
            colorChecked = true;
            PlayerTwoBlackRadioBtn.IsChecked = true;
        }

        private void PlayerOneBlackRadioBtn_Click(object sender, RoutedEventArgs e)
        {
            colorChecked = true;
            PlayerTwoWhiteRadioBtn.IsChecked = true;
        }

        private void PlayerTwoWhiteRadioBtn_Click(object sender, RoutedEventArgs e)
        {
            colorChecked = true;
            PlayerOneBlackRadioBtn.IsChecked = true;
        }

        private void PlayerTwoBlackRadioBtn_Click(object sender, RoutedEventArgs e)
        {
            colorChecked = true;
            PlayerOneWhiteRadioBtn.IsChecked = true;
        }

        private void StartBtn_Click(object sender, RoutedEventArgs e)
        {
            if (VerifyPlayersTypes() && VerifyPlayerNames())
            {
                this.Close();
                mre.Set();
            }
        }
        private bool VerifyPlayersTypes()
        {
            bool playertypesCorrect = true;
            if ((bool)PlayerOneHumanRadioBtn.IsChecked)
                player1 = "Human";
            else if ((bool)PlayerOneComputerRadioBtn.IsChecked)
                player1 = "Computer";
            else if ((bool)PlayerOneRemoteRadioBtn.IsChecked)
                player1 = "RemoteComputer";
            else
            {
                playertypesCorrect = false;
                statBarText.Text = "Must choose a player type for Player One";
            }

            if ((bool)PlayerTwoHumanRadioBtn.IsChecked)
                player2 = "Human";
            else if ((bool)PlayerTwoComputerRadioBtn.IsChecked)
                player2 = "Computer";
            else if ((bool)PlayerTwoRemoteRadioBtn.IsChecked)
                player2 = "RemoteComputer";
            else
            {
                playertypesCorrect = false;
                statBarText.Text = "Must choose a player type for Player Two";
            }

            if (playertypesCorrect)
            {
                if ((bool)PlayerOneWhiteRadioBtn.IsChecked)
                    PlayerOneWhite = true;
            }
            return playertypesCorrect;
        }
        private bool VerifyPlayerNames()
        {
            bool playerNameCorrect = false;
            if (PlayerOneNameTxtBox.Text == "")
                statBarText.Text = "Player One has no name";
            else if (PlayerTwoNameTxtBox.Text == "")
                statBarText.Text = "Player Two has no name";
            else if (PlayerOneNameTxtBox.Text == PlayerTwoNameTxtBox.Text)
                statBarText.Text = "Can't choose same name";
            else if (!colorChecked)
                statBarText.Text = "Must choose a color";
            else
            {
                PlayerOneName = PlayerOneNameTxtBox.Text;
                PlayerTwoName = PlayerTwoNameTxtBox.Text;
                playerNameCorrect = true;
            }
            return playerNameCorrect;
        }
    }
}
